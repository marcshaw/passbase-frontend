## TODO

* Write JEST tests
* Use the User info end point for the new transaction

## To Run

* docker-compose build
* docker-compose up

Navigate to localhost:3000

Login info:
* jimmy@jim.com / jimjim
* marc@marc.com / marcmarc

Registration is at `/register`, but note the todo, I need to create an endpoint to get the user information and as such, I have just hard coded one user to send transactions too.
