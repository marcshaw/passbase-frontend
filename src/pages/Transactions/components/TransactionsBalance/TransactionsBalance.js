import React from "react";
import {
  Table,
  Header,
  Icon
} from "semantic-ui-react";

function TransactionsBalance(props) {
  const { balances } = props;

  return (
    <Table basic="very" celled collapsing>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Currency</Table.HeaderCell>
          <Table.HeaderCell>Amount</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        { balances.map((balance, index) => {
          const { currency, amount } = balance;

          return (
            <Table.Row key={index}>
              <Table.Cell>
                <Header as="h4" image>
                  <Icon name={currency.toLowerCase()} />
                  <Header.Content>{ currency } </Header.Content>
                </Header>
              </Table.Cell>
              <Table.Cell>{ amount }</Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
}

 export default TransactionsBalance;