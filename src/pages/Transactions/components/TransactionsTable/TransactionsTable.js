import React from "react";
import {
  Icon,
  Table,
  Button,
} from "semantic-ui-react";
import { withRouter } from 'react-router-dom';

function TransactionsTable(props) {
  const { transactions } = props;

  return (
    <Table compact celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>ID</Table.HeaderCell>
          <Table.HeaderCell>Sender</Table.HeaderCell>
          <Table.HeaderCell>Receiver</Table.HeaderCell>
          <Table.HeaderCell>Sender Amount</Table.HeaderCell>
          <Table.HeaderCell>Receiver Amount</Table.HeaderCell>
          <Table.HeaderCell>Sender Currency</Table.HeaderCell>
          <Table.HeaderCell>Receiver Currency</Table.HeaderCell>
          <Table.HeaderCell>Exchange Rate</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {transactions.map((transaction, index) => {
          console.log(transaction);
          return (
            <Table.Row key={index}>
              <Table.Cell>{transaction.id}</Table.Cell>
              <Table.Cell>{transaction.sender_name}</Table.Cell>
              <Table.Cell>{transaction.receiver_name}</Table.Cell>
              <Table.Cell>{transaction.sender_amount}</Table.Cell>
              <Table.Cell>{transaction.receiver_amount}</Table.Cell>
              <Table.Cell>{transaction.sender_currency}</Table.Cell>
              <Table.Cell>{transaction.receiver_currency}</Table.Cell>
              <Table.Cell>{transaction.exchange_rate}</Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
      <Table.Footer fullWidth>
        <Table.Row>
          <Table.HeaderCell colSpan="10">
            <Button
              floated="right"
              icon
              labelPosition="left"
              primary
              size="small"
              onClick={() => props.history.push("/transactions/new")}
            >
              <Icon name="user" /> New Transaction
            </Button>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Footer>
    </Table>
  );
}

export default withRouter(TransactionsTable);
