import React, { Fragment } from 'react';
import { Divider, Header, Icon, Loader } from 'semantic-ui-react';

import { TransactionsTable, TransactionsBalance } from './components';
import { useLoadTransactions, useLoadBalances } from '../../helpers';

function Transactions(props) {
  const transactionRequest = useLoadTransactions(),
    loadingTransactions = transactionRequest.loading,
    transactions = transactionRequest.transactions,

    balancesRequest = useLoadBalances(),
    loadingBalances = balancesRequest.loading,
    balances = balancesRequest.balances;

    return (
      <React.Fragment>
        <Divider horizontal>
          <Header as="h4">
            <Icon name="money" />
            Balances
          </Header>
        </Divider>

        {loadingBalances && <Loader active inline="centered" />}
        {!loadingBalances && (
          <Fragment>
            <TransactionsBalance balances={balances} />
          </Fragment>
        )}

        <Divider horizontal>
          <Header as="h4">
            <Icon name="bar chart" />
            Transactions
          </Header>
        </Divider>

        {loadingTransactions && <Loader active inline="centered" />}
        {!loadingTransactions && (
          <Fragment>
            <TransactionsTable transactions={transactions} />
          </Fragment>
        )}
      </React.Fragment>
    );
}

export default Transactions;