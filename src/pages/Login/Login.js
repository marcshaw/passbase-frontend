import React from 'react';
import useForm from 'react-hook-form';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { Form } from 'semantic-ui-react'

function Login(props) {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = data => {
    console.log(data);
    axios
      .post("http://localhost:3001/login", {
        user: {
          email: data.Email,
          password: data.Password
        },
        withCredentials: true
      })
      .then(function(response) {
        localStorage.setItem("userInfo", response.headers.authorization);
        props.history.push("/transactions");
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Field>
        <input
          type="text"
          placeholder="Email"
          name="Email"
          ref={register({
            required: "Email Required",
            pattern: {
              value: /^\S+@\S+$/i,
              message: "Email does not match pattern"
            }
          })}
        />
        {errors.Email && errors.Email.message}
      </Form.Field>

      <Form.Field>
        <input
          type="password"
          placeholder="Password"
          name="Password"
          ref={register({ required: "Password Required" })}
        />
        {errors.Password && errors.Password.message}
      </Form.Field>

      <input type="submit" />
    </Form>
  );

}

export default withRouter(Login);