import React, { useState } from "react";
import useForm from 'react-hook-form';
import { Form } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';

import { saveTransaction } from '../../helpers';

function Transaction(props) {
  const { register, handleSubmit, errors } = useForm(),
    [formError, setFormError] = useState(),
    onSubmit = async data => {
      const { error } = await saveTransaction(data);

      if (error == null) {
        props.history.push("/transactions");
      } else {
        setFormError(error);
      }
    };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Field>
        <label>Receiver</label>
        <select
          name="Receiver"
          ref={register({ required: "Receiver is required" })}
        >
          <option value="3">Marc</option>
        </select>
        {errors.Receiver && errors.Receiver.message}
      </Form.Field>
      <Form.Field>
        <label>From Currency</label>
        <select name="From Currency" ref={register({ required: true })}>
          <option value="USD">USD</option>
          <option value="GBP">GBP</option>
          <option value="EUR">EUR</option>
        </select>
      </Form.Field>
      <Form.Field>
        <label>To Currency</label>
        <select name="To Currency" ref={register({ required: true })}>
          <option value="USD">USD</option>
          <option value="GBP">GBP</option>
          <option value="EUR">EUR</option>
        </select>
      </Form.Field>
      <Form.Field>
        <label>Amount</label>
        <input
          type="number"
          placeholder="Amount"
          name="Amount"
          ref={register({
            required: "Amount is required",
            min: { message: "Needs to be more than 1", value: 1 }
          })}
        />
        {errors.Amount && errors.Amount.message}
      </Form.Field>

      <input type="submit" />
      {formError && `Something went wrong! ${formError}`}
    </Form>
  );
}

export default withRouter(Transaction);
