import { useEffect, useState } from "react";
import axios from "axios";

function useLoadTransactions() {
  const current = localStorage.getItem("userInfo"),
    [transactionRequest, setTransactionRequest] = useState({
      loading: true,
      transactions: {}
    });

  useEffect(() => {
    axios
      .get("http://localhost:3001/transactions", {
        headers: { Authorization: current }
      })
      .then(function(response) {
        setTransactionRequest({ loading: false, transactions: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }, [current]);

  return transactionRequest;
}

export default useLoadTransactions;