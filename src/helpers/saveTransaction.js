import axios from "axios";

function saveTransaction(data) {
  const current = localStorage.getItem("userInfo");

  console.log(data);
  return axios
    .post(
      "http://localhost:3001/transactions",
      {
        transaction: {
          sender_currency: data['From Currency'],
          receiver_currency: data['To Currency'],
          sender_amount: data.Amount,
          receiver: data.Receiver
        },

      },
      { headers: { Authorization: current }, withCredentials: true }
    )
    .then(function(response) {
      return { error: null };
    })
    .catch(function(error) {
      return { error: error.response.data.errors };
    });
}

export default saveTransaction;