import { useEffect, useState } from "react";
import axios from "axios";

function useLoadBalances() {
  const current = localStorage.getItem("userInfo"),
    [balanceRequest, setBalanceRequest] = useState({
      loading: true,
      balances: {}
    });

  useEffect(() => {
    axios
      .get("http://localhost:3001/balance", {
        headers: { Authorization: current }
      })
      .then(function(response) {
        setBalanceRequest({ loading: false, balances: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }, [current]);

  return balanceRequest;
}

export default useLoadBalances;