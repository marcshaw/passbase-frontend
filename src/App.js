import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { Login, Register, Transactions, Transaction } from './pages'

export default function App() {
  return (
    <Router>
        <Switch>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/transactions/new">
            <Transaction />
          </Route>
          <Route path="/transactions">
            <Transactions />
          </Route>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
    </Router>
  );
}

